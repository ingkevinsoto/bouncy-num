

def found99(percent):
    total = 0
    number = 1
    count = 0

    while total < percent:
        if not is_decrease(number) and not is_increase(number) and number >= 100:
            count += 1
        total = (float(count) / float(number)) * float(100)
        number += 1
    print "percent ", total

    print "number ", number-1


def is_increase(number):
    list_number = [n for n in str(number)]
    list_aux = []

    for idx, val in enumerate(list_number):

        if idx != len(list_number)-1:
            if int(val) <= int(list_number[idx+1]):
                list_aux.append(val)
        else:
            if int(val) >= int(list_number[idx-1]):
                list_aux.append(val)

    return len(list_number) == len(list_aux)


def is_decrease(number):
    list_number = [n for n in str(number)]
    list_aux = []

    for idx, val in enumerate(list_number):

        if idx != len(list_number) - 1:
            if int(val) >= int(list_number[idx + 1]):
                list_aux.append(val)
        else:
            if int(val) <= int(list_number[idx - 1]):
                list_aux.append(val)

    return len(list_number) == len(list_aux)


found99(99)
